package ro.utcn.main;

import ro.utcn.simulation.SimulationData;
import ro.utcn.simulation.SimulationManager;

import java.io.FileWriter;

public class Main {

    public static void main(String[] args) {
        try {
            final SimulationData simulationData = new SimulationData(args[0]);
            final FileWriter fileWriter = new FileWriter(args[1]);
            final SimulationManager simulationManager = new SimulationManager(simulationData, fileWriter);
            final Thread t = new Thread(simulationManager);
            t.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
