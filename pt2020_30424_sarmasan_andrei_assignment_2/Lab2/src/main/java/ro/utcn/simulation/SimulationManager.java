package ro.utcn.simulation;

import ro.utcn.model.ClientModel;
import ro.utcn.model.QueueModel;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SimulationManager implements Runnable {

    private SimulationData simulationData;
    private List<QueueModel> queueModels = new ArrayList<QueueModel>();
    private List<ClientModel> clientModels;

    private boolean allClientsServed = false;
    private FileWriter fileWriter;

    public SimulationManager(final SimulationData simulationData, FileWriter fileWriter) {
        this.fileWriter = fileWriter;
        this.simulationData = simulationData;
        final List<ClientModel> clients = new ArrayList<ClientModel>();
        for(int index = 0; index < simulationData.getNumberOfClients(); index++) {
            clients.add(new ClientModel(index + 1, simulationData.getMinimumArrivalTime(), simulationData.getMaximumArrivalTime(),
                                                 simulationData.getMinimumServiceTime(), simulationData.getMaximumServiceTime()));
        }
        clientModels = Collections.synchronizedList(clients);
        for(int index = 0; index < simulationData.getNumberOfQueues(); index++) {
            final QueueModel queueModel = new QueueModel(fileWriter);
            queueModel.setId(index + 1);
            Thread t = new Thread(queueModel);
            t.start();
            queueModels.add(queueModel);
        }
    }

    private void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void assignClient(int currentTime) {
        for(ClientModel clientModel : clientModels) {
            if(clientModel.getArrivalTime() == currentTime && !clientModel.isTaken()) {
                int bestQueueTime = 1000;
                QueueModel bestQueue = null;
                for(QueueModel queueModel : queueModels) {
                    if (queueModel.getTotalTime() < bestQueueTime) {
                        bestQueueTime = queueModel.getTotalTime();
                        bestQueue = queueModel;
                    }
                }
                if(bestQueue.getQueue().isEmpty()) {
                    clientModel.setServiceTime(clientModel.getServiceTime() + 1);
                }
                bestQueue.getQueue().add(clientModel);
                bestQueue.setTotalTime(bestQueueTime + clientModel.getServiceTime());
                clientModel.setTaken(true);
            }
        }
    }

    private void printClients(int currentTime) throws IOException {
        allClientsServed = true;
        fileWriter.write("Time " + currentTime + "\n");
        fileWriter.write("Waiting: ");
        for(ClientModel clientModel : clientModels) {
            if(!clientModel.isTaken()) {
                fileWriter.write(clientModel + " ");
            }
            if(clientModel.getServiceTime() != 0) {
                allClientsServed = false;
            }
        }
        fileWriter.write("\n");
        sleep();
    }

    public void run() {
        sleep();
        int simulationTime = simulationData.getSimulationInterval();
        int currentTime = 0;
        while (currentTime++ < simulationTime && !allClientsServed) {
            assignClient(currentTime);
            sleep();
            try {
                printClients(currentTime);
            } catch (IOException e) {
                e.printStackTrace();
            }
            sleep();
        }
        int totalAverage = 0;
        for(QueueModel queueModel: queueModels) {
            queueModel.setActive(false);
            totalAverage += queueModel.getAverage().get();
        }
        try {
            fileWriter.write("Average waiting time: " + (double) totalAverage / simulationData.getNumberOfClients() + "\n");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
