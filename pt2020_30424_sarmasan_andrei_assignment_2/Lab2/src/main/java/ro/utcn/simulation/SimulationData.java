package ro.utcn.simulation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SimulationData {

    private Integer numberOfClients;

    private Integer numberOfQueues;

    private Integer simulationInterval;

    private Integer minimumArrivalTime;

    private Integer maximumArrivalTime;

    private Integer minimumServiceTime;

    private Integer maximumServiceTime;

    public SimulationData(final String path) throws FileNotFoundException {
        File file = new File(path);
        Scanner scanner = new Scanner(file);
        numberOfClients = scanner.nextInt();
        numberOfQueues = scanner.nextInt();
        simulationInterval = scanner.nextInt();
        scanner.nextLine();
        final String[] arrivalTime = scanner.nextLine().split(",");
        minimumArrivalTime = Integer.valueOf(arrivalTime[0]);
        maximumArrivalTime = Integer.valueOf(arrivalTime[1]);
        final String[] serviceTime = scanner.nextLine().split(",");
        minimumServiceTime = Integer.valueOf(serviceTime[0]);
        maximumServiceTime = Integer.valueOf(serviceTime[1]);
        scanner.close();
    }

    public Integer getNumberOfClients() {
        return numberOfClients;
    }

    public void setNumberOfClients(Integer numberOfClients) {
        this.numberOfClients = numberOfClients;
    }

    public Integer getNumberOfQueues() {
        return numberOfQueues;
    }

    public void setNumberOfQueues(Integer numberOfQueues) {
        this.numberOfQueues = numberOfQueues;
    }

    public Integer getSimulationInterval() {
        return simulationInterval;
    }

    public void setSimulationInterval(Integer simulationInterval) {
        this.simulationInterval = simulationInterval;
    }

    public Integer getMinimumArrivalTime() {
        return minimumArrivalTime;
    }

    public void setMinimumArrivalTime(Integer minimumArrivalTime) {
        this.minimumArrivalTime = minimumArrivalTime;
    }

    public Integer getMaximumArrivalTime() {
        return maximumArrivalTime;
    }

    public void setMaximumArrivalTime(Integer maximumArrivalTime) {
        this.maximumArrivalTime = maximumArrivalTime;
    }

    public Integer getMinimumServiceTime() {
        return minimumServiceTime;
    }

    public void setMinimumServiceTime(Integer minimumServiceTime) {
        this.minimumServiceTime = minimumServiceTime;
    }

    public Integer getMaximumServiceTime() {
        return maximumServiceTime;
    }

    public void setMaximumServiceTime(Integer maximumServiceTime) {
        this.maximumServiceTime = maximumServiceTime;
    }
}
