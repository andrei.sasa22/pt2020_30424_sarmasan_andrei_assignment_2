package ro.utcn.model;

import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class QueueModel implements Runnable {

    private BlockingQueue<ClientModel> queue = new ArrayBlockingQueue<ClientModel>(100);

    private Integer totalTime = 0;

    private boolean active = true;

    private Integer id;

    private AtomicInteger average = new AtomicInteger(0);

    private FileWriter fileWriter;

    public QueueModel(FileWriter fileWriter) {
        this.fileWriter = fileWriter;
    }

    private void sleep() {
        try {
            Thread.sleep(1000 + id);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void printQueue() throws IOException {
        sleep();
        fileWriter.write("Queue " + id + ": ");
        if(queue.isEmpty()) {
            fileWriter.write("none");
        }
        for(ClientModel client: queue) {
            fileWriter.write(client + " ");
        }
        fileWriter.write("\n");
    }

    public void run() {
        sleep();
        while (active) {
            if(!queue.isEmpty()) {
                if(queue.element().getServiceTime() == 1) {
                    try {
                        queue.element().setServiceTime(0);
                        queue.take();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    queue.element().updateServiceTime();
                    totalTime--;
                }
            }
            sleep();
            try {
                printQueue();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            average.set(average.get() + queue.size());
            sleep();
        }
    }

    public BlockingQueue<ClientModel> getQueue() {
        return queue;
    }

    public void setQueue(BlockingQueue<ClientModel> queue) {
        this.queue = queue;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Integer totalTime) {
        this.totalTime = totalTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AtomicInteger getAverage() {
        return average;
    }

    public void setAverage(AtomicInteger average) {
        this.average = average;
    }
}
