package ro.utcn.model;

import java.util.Random;

public class ClientModel {

    private Integer id;

    private Integer arrivalTime;

    private Integer serviceTime;

    private boolean taken = false;

    public ClientModel(Integer id, Integer minArrivalTime,  Integer maxArrivalTime,
                       Integer minServiceTime, Integer maxServiceTime) {
        this.id = id;
        this.arrivalTime = (new Random()).nextInt(maxArrivalTime - minArrivalTime) + minArrivalTime;
        this.serviceTime = (new Random()).nextInt(maxServiceTime - minServiceTime) + minServiceTime;
    }

    public void updateServiceTime() {
        serviceTime = serviceTime - 1;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Integer arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Integer getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(Integer serviceTime) {
        this.serviceTime = serviceTime;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    @Override
    public String toString() {
        return "{" +
                "" + id +
                ", " + arrivalTime +
                ", " + serviceTime +
                '}';
    }
}
